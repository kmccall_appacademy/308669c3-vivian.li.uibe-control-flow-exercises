# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.reject { |el| el.downcase == el }.join

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.even? ? str[str.length / 2 - 1, 2] : str[str.length / 2]

end

# Return the number of vowels in a string.
def num_vowels(str)
  vowel = %w(a e i o u)
  str.chars.count { |el| vowel.include?(el.downcase) }

end

# Return the factorial of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(1, :*)

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = '')
  result = arr.map { |el| el + separator }.join
  separator.empty? ? result : result[0...-separator.length]

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |el, idx|
    idx.even? ? el.downcase : el.upcase
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |el|
    el.length >= 5 ? el.reverse : el
  end.join(' ')

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |num|
    if (num % 3).zero? && (num % 5).zero?
      'fizzbuzz'
    elsif (num % 3).zero?
      'fizz'
    elsif (num % 5).zero?
      'buzz'
    else
      num
    end
  end
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (1..num).count { |el| (num % el).zero? } == 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |el| (num % el).zero? }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor = factors(num)
  (1..num).select { |el| prime?(el) && factor.include?(el) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |el|
    el.even? ? even << el : odd << el
  end
  even.count > 1 ? odd[0] : even[0]
end
